package org.districraft.auction_backend_demo.model;


public class Setting {


    private int id;


    private String settingKey;

    private String settingValue;

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public String getSettingKey() {
        return settingKey;
    }

    public void setSettingKey(String settingKey) {
        this.settingKey = settingKey;
    }

    public String getSettingValue() {
        return settingValue;
    }

    public void setSettingValue(String settingValue) {
        this.settingValue = settingValue;
    }

    @Override
    public String toString() {
        return String.format("#<Setting.class>(id=%d, settingKey=%s, settingValue=%s)", id, settingKey, settingValue);
    }
}
