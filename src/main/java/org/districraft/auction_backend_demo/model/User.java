package org.districraft.auction_backend_demo.model;

import org.mindrot.jbcrypt.BCrypt;

public class User {
    private int id;
    private String username;
    private String uuid;
    private String password_hash;

    public User(int id, String username, String uuid, String password_hash) {
        this.id = id;
        this.username = username;
        this.uuid = uuid;
        this.password_hash = password_hash;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getPassword_hash() {
        return password_hash;
    }

    public void setPassword_hash(String password_hash) {
        this.password_hash = password_hash;
    }

    public boolean authenticate(String password) {
        return BCrypt.checkpw(password, password_hash);
    }
}
