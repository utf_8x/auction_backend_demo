package org.districraft.auction_backend_demo;

import org.districraft.auction_backend_demo.sql.ConnectionBuilder;
import org.jooq.DSLContext;

import java.sql.SQLException;

public class ApplicationMain {

    public static DSLContext context;

    public static void main(String... args) throws SQLException {
        String username = "auction";
        String password = "securepass";
        ConnectionBuilder contextBuilder = new ConnectionBuilder(username, password, null);
        Runtime.getRuntime().addShutdownHook(new ShutdownThread(contextBuilder));

        JettyServer server = new JettyServer();
        try {
            server.start();
        } catch (Exception e) {
            e.printStackTrace();
        }

        context = contextBuilder.get();
    }
}
