package org.districraft.auction_backend_demo.controller;

import org.districraft.auction_backend_demo.ApplicationMain;
import org.districraft.auction_backend_demo.Controller;
import org.districraft.auction_backend_demo.annotation.AdminRequired;
import org.districraft.auction_backend_demo.annotation.LoginRequired;
import org.districraft.auction_backend_demo.annotation.Route;
import org.districraft.auction_backend_demo.util.GarbageCollector;
import org.districraft.auction_backend_demo.util.Response;
import org.jooq.Record;
import org.jooq.Result;
import org.mindrot.jbcrypt.BCrypt;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static java.util.Map.entry;

public class ApiController extends Controller {

    @Route("/")
    public void getIndex(HttpServletRequest request, Response response) throws IOException {
        response.renderJson(Map.ofEntries(
                entry("status", "OK"),
                entry("version", "1")
        ));
    }

    @Route("/settings")
    @LoginRequired
    @AdminRequired
    public void getSettings(HttpServletRequest request, Response response) throws IOException {
        ApplicationMain.context.select().from("settings").fetch();
        Result<Record> res = ApplicationMain.context.select().from("settings").fetch();

        HashMap<String, String> rslt = new HashMap<>();

        for(Record r : res) {
            int id = (int) r.getValue("id", Integer.class);
            String settingKey = (String) r.getValue("settingKey");
            String settingValue = (String) r.getValue("settingValue");
            rslt.put(settingKey, settingValue);
        }
        response.renderJson(rslt);
    }

    @Route("/status")
    @LoginRequired
    @AdminRequired
    public void getStatus(HttpServletRequest request, Response response) throws IOException {
        Runtime r = Runtime.getRuntime();
        HashMap<Object, Object> sysStats = new HashMap<>();
        HashMap<Object, Object> res = new HashMap<>();

        long freeMem = r.freeMemory() / (1024L*1024L);
        long maxMem = r.maxMemory() / (1024L*1024L);
        long totalMem = r.totalMemory() / (1024L*1024L);
        long usedMem = totalMem - freeMem;

        sysStats.put("cpu", String.valueOf(r.availableProcessors()));
        sysStats.put("free_memory", String.valueOf(freeMem));
        sysStats.put("max_memory", String.valueOf(maxMem));
        sysStats.put("total_memory", String.valueOf(totalMem));
        sysStats.put("used_memory", String.valueOf(usedMem));

        res.put("system_stats", sysStats);

        response.renderJson(res);
    }

    @Route("/garbage_collection")
    @LoginRequired
    @AdminRequired
    public void collectGarbage(HttpServletRequest request, Response response) throws IOException {
        long start = System.nanoTime();

        GarbageCollector.forceCollection();

        long finish = System.nanoTime();
        long elapsed = (finish - start) / 1000000;

        response.renderJson(Map.ofEntries(
                entry("status", "ok"),
                entry("elapsed_time", elapsed)
        ));
    }

}
//
//    Result<Record> res = ApplicationMain.context.select().from("users").fetch();
//
//    ArrayList<Object> users = new ArrayList<>();
//
//        for(Record r : res) {
//                users.add(r.getValue("username"));
//                }
