package org.districraft.auction_backend_demo.sql;

import org.jooq.DSLContext;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionBuilder {
    private Connection conn;
    private DSLContext context;
    private String url = "jdbc:mysql://localhost:3306/auctiondemo?serverTimezone=UTC";

    public ConnectionBuilder(String username, String password, String url) throws SQLException {
        if(url == null) {
            url = this.url;
        }
        conn = DriverManager.getConnection(url, username, password);
        context = DSL.using(conn, SQLDialect.MYSQL);
    }

    public DSLContext get() {
        return this.context;
    }

    public void cleanup() throws SQLException {
        this.conn.close();
    }
}
