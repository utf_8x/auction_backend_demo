package org.districraft.auction_backend_demo;

import org.districraft.auction_backend_demo.controller.ApiController;
import org.districraft.auction_backend_demo.util.MappingBuilder;

import java.util.HashMap;
import java.util.Map;

import static org.districraft.auction_backend_demo.util.MappingBuilder.route;

class Routes {
    Routes() {
        route("/api/v1/*", ApiController.class);
    }

    Map<String, Class<?>> build() {
        return MappingBuilder.getAll();
    }
}