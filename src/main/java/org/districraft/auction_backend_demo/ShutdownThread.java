package org.districraft.auction_backend_demo;

import org.districraft.auction_backend_demo.sql.ConnectionBuilder;

import java.sql.SQLException;

public class ShutdownThread extends Thread {
    ConnectionBuilder builder;

    public ShutdownThread(ConnectionBuilder builder) {
        this.builder = builder;
    }

    public void run() {
        try {
            builder.cleanup();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
