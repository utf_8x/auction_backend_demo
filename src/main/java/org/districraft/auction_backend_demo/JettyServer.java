package org.districraft.auction_backend_demo;

import org.eclipse.jetty.server.Connector;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.servlet.ServletHandler;

import java.util.HashMap;
import java.util.Map;

import static java.util.Map.entry;

public class JettyServer {
    private Server server;

    public void start() throws Exception {
        server = new Server();
        ServerConnector connector = new ServerConnector(server);
        connector.setPort(8090);
        server.setConnectors(new Connector[] {connector});
        ServletHandler handler = new ServletHandler();

        for(Map.Entry<String, Class<?>> entry : new Routes().build().entrySet()) {
            handler.addServletWithMapping((Class) entry.getValue(), entry.getKey());

        }

        server.setHandler(handler);
        server.start();
    }
}
