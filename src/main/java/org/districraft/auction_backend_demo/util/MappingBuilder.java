package org.districraft.auction_backend_demo.util;

import java.util.HashMap;
import java.util.Map;

public class MappingBuilder {
    private static HashMap<String, Class<?>> mappings = new HashMap<String, Class<?>>();

    public static void route(String path, Class<?> clazz) {
        mappings.put(path, clazz);
    }

    public static HashMap<String, Class<?>> getAll() {
        return mappings;
    }

    public static Class<?> getOne(String key) {
        return mappings.get(key);
    }
}
