package org.districraft.auction_backend_demo.util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.jooq.tools.json.JSONObject;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class Response {
    HttpServletResponse responseObject;
    int statusCode = 200;

    public Response(HttpServletResponse response) {
        this.responseObject = response;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public int getStatusCode() {
        return this.statusCode;
    }

    public void renderJson(Object input) throws IOException {
        Gson s = new GsonBuilder().create();
        String payload = s.toJson(input);
        responseObject.getWriter().println(payload);
        responseObject.setContentType("application/json");
        responseObject.setStatus(statusCode);
    }
}
