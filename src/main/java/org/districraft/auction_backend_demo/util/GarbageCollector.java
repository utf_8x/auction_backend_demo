package org.districraft.auction_backend_demo.util;

import java.lang.ref.WeakReference;

public class GarbageCollector {
    public static void forceCollection() {
        Object obj = new Object();
        WeakReference ref = new WeakReference<Object>(obj);
        obj = null;
        while(ref.get() != null) {
            System.gc();
        }
    }
}
