package org.districraft.auction_backend_demo;

import org.districraft.auction_backend_demo.annotation.AdminRequired;
import org.districraft.auction_backend_demo.annotation.LoginRequired;
import org.districraft.auction_backend_demo.annotation.Route;
import org.districraft.auction_backend_demo.util.Response;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class Controller extends HttpServlet {

    //private Map<String, Method> mapping = new HashMap<String, Method>();
    private Map<String, Map<String, Object>> mapping = new HashMap<>();

    public Controller() {

        for(Method m : this.getClass().getDeclaredMethods()) {
            Route routeAnnotation = m.getAnnotation(Route.class);
            LoginRequired loginAnnotation = m.getAnnotation(LoginRequired.class);
            AdminRequired adminAnnotation = m.getAnnotation(AdminRequired.class);

            if(routeAnnotation != null) {
                Map<String, Object> lmap = new HashMap<>();

                lmap.put("controller", m);

                if(loginAnnotation != null) {
                    lmap.put("login", true);
                } else {
                    lmap.put("login", false);
                }

                if(adminAnnotation != null) {
                    lmap.put("admin", true);
                } else {
                    lmap.put("admin", false);
                }

                mapping.put(routeAnnotation.value(), lmap);
            }
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String route = request.getPathInfo();

        if(route == null || route.equalsIgnoreCase("")) route = "/";



        Map<String, Object> mapped = mapping.get(route);

        if(mapped == null || mapped.getOrDefault("controller", null) == null) {
            response.setStatus(404);
            response.getWriter().println("404 - Server error");
            return;
        }

        try {

            if((boolean) mapped.getOrDefault("login", false)) {
                if(!performLoginChecks(request, response)) {
                    response.setStatus(403);
                    response.getWriter().println("403 - Unauthorized");
                    return;
                }
            }

            Method controller = (Method) mapped.get("controller");
            controller.invoke(this, request, new Response(response));
        } catch (IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
            response.setStatus(500);
            response.getWriter().println("500 - Server error");
        }
    }

    private boolean performLoginChecks(HttpServletRequest request, HttpServletResponse response) {
        return true;
    }
}
